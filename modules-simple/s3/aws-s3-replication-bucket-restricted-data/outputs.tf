output "replication_bucket_name" {
  value = aws_s3_bucket.replication_bucket.id
}

output "replication_bucket_arn" {
  value = aws_s3_bucket.replication_bucket.arn
}