provider "aws" {
    region = var.replication-bucket-region
}

resource "aws_s3_bucket" "replication_bucket" {
    bucket = var.replication-bucket-name
}

resource "aws_s3_bucket_versioning" "versionsing" {
  bucket = aws_s3_bucket.replication_bucket.id
  versioning_configuration {
    status = var.enable-bucket-versioning
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_encryption_configuration" {
  bucket = aws_s3_bucket.replication_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# Block Public Access - Restricted Bucket
resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.replication_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.replication_bucket.id

  topic {
    topic_arn     = var.sns-bucket-notification-topic-arn
    events        = ["s3:ObjectCreated:Copy", "s3:ObjectRemoved:*"]
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.replication_bucket.id
  policy = templatefile("${path.module}/template/s3_bucket_policy.json",
    { bucket_name = aws_s3_bucket.replication_bucket.bucket  })
}

# Lifecycle rule
resource "aws_s3_bucket_lifecycle_configuration" "bucket_lifecycle" {
  bucket = aws_s3_bucket.replication_bucket.id

  rule {
    id     = var.lifecycle-rule-id
    status = "Enabled"

    filter {
      prefix = "" # Apply to all objects in the bucket
    }

    noncurrent_version_transition {
      noncurrent_days = 30
      storage_class   = "INTELLIGENT_TIERING"
    }

  }
}
