variable "replication-bucket-name" {
  type    = string
  default = "Name of the bucket to replicate to"
}

variable "enable-bucket-versioning" {
  type        = string
  description = "Turn versioning on or off for the restricted bucket"
  default     = "Enabled"
}

variable "sns-bucket-notification-topic-arn" {
  type        = string
  description = "arn of the SNS topic"
}

variable "lifecycle-rule-id" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Archive for cheaper storage"
}

variable "replication-bucket-region" {
  type = string
  default = "us-west-2"
}