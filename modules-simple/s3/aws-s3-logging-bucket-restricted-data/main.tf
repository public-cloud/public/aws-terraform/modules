resource "aws_s3_bucket" "log_bucket" {
  bucket = var.server-access-logging-bucket-name
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.log_bucket.id
  policy = templatefile("${path.module}/template/s3_bucket_policy.json",
    { bucket_name = aws_s3_bucket.log_bucket.bucket  })
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_encryption_configuration" {
  bucket = aws_s3_bucket.log_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "bucket_public_access_block" {
  bucket = aws_s3_bucket.log_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.log_bucket.id

  topic {
    topic_arn     = var.sns-bucket-notification-topic-arn
    events        = ["s3:ObjectCreated:Copy", "s3:ObjectRemoved:*"]
  }
}

# Set Intelligent Tiering
resource "aws_s3_bucket_lifecycle_configuration" "log_bucket_lifecycle_archive" {
  bucket = aws_s3_bucket.log_bucket.id

  rule {
    id     = var.lifecycle-rule-id-cheaper
    status = "Enabled"

    filter {
      prefix = "" # Apply to all objects in the bucket
    }

    noncurrent_version_transition {
      noncurrent_days = 30
      storage_class   = "INTELLIGENT_TIERING"
    }
  }
  
  rule {
    id     = var.lifecycle-rule-id-deletion
    status = "Enabled"

    filter {
      prefix = "" # Apply to all objects in the bucket
    }

    expiration {
        expired_object_delete_marker = true
    }
    
    abort_incomplete_multipart_upload {
        days_after_initiation = 450
    }
  }
}
