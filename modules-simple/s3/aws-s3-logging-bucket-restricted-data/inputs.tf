variable "server-access-logging-bucket-name" {
  type        = string
  description = "Name of the bucket to store access logs"
}

variable "sns-bucket-notification-topic-arn" {
  type        = string
  description = "arn of the SNS topic"
}

variable "lifecycle-rule-id-cheaper" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Archive for cheaper storage"
}

variable "lifecycle-rule-id-deletion" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Deletion for cheaper storage"
}
