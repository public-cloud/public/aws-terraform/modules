variable "enable-bucket-versioning" {
  type        = string
  description = "Turn versioning on or off for the restricted bucket"
  default     = "Enabled"
}

variable "restricted-bucket-name" {
  type        = string
  description = "restricted bucket name"
}

variable "sns-bucket-notification-topic-arn" {
  type        = string
  description = "arn of the SNS topic"
  
}

variable "replication-bucket-name" {
  type    = string
  default = "Name of the bucket to replicate to"
}

variable "replication-bucket-arn" {
  type = string
  default = "arn of the bucket to replicate to"
}

variable "log-bucket-name" {
  type    = string
  default = "Name of the bucket to log to"
}
