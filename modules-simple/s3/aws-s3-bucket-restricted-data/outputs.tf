output "restricted_bucket_name" {
  value = aws_s3_bucket.restricted_bucket.id
}

output "restricted_bucket_arn" {
  value = aws_s3_bucket.restricted_bucket.arn
}