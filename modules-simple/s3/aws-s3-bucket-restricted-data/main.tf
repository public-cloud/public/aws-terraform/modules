resource "aws_s3_bucket" "restricted_bucket" {
  bucket = var.restricted-bucket-name
}


# Enable versioning (bool)
resource "aws_s3_bucket_versioning" "restricted_bucket_versioning" {
  bucket = aws_s3_bucket.restricted_bucket.id
  versioning_configuration {
    status = var.enable-bucket-versioning
  }
}

# Turn on S3 Encryption - Restricted Bucket
resource "aws_s3_bucket_server_side_encryption_configuration" "restricted_bucket_encryption_configuration" {
  bucket = aws_s3_bucket.restricted_bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

# Block Public Access - Restricted Bucket
resource "aws_s3_bucket_public_access_block" "restricted_bucket_public_access_block" {
  bucket = aws_s3_bucket.restricted_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.restricted_bucket.id

  topic {
    topic_arn     = var.sns-bucket-notification-topic-arn
    events        = ["s3:ObjectRemoved:*", "s3:LifecycleTransition", "s3:LifecycleExpiration:*", "s3:IntelligentTiering", "s3:Replication:OperationFailedReplication" ]
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.restricted_bucket.id
  policy = templatefile("${path.module}/template/s3_bucket_policy.json",
    { bucket_name = aws_s3_bucket.restricted_bucket.bucket  })
}

# Lifecycle rule
resource "aws_s3_bucket_lifecycle_configuration" "restricted_bucket_lifecycle" {
  bucket = aws_s3_bucket.restricted_bucket.id

  rule {
    id     = "Archive for cheaper storage"
    status = "Enabled"

    filter {
      prefix = "" # Apply to all objects in the bucket
    }

    transition {
      days          = 100
      storage_class = "INTELLIGENT_TIERING"
    }

    noncurrent_version_transition {
      noncurrent_days = 30
      storage_class   = "INTELLIGENT_TIERING"
    }

  }
}

# TODO Replication Rule

resource "aws_iam_role" "replication" {
  name               = "${var.replication-bucket-name}-role"
  assume_role_policy = file("${path.module}/template/replication_trust_relationship.json")
}

resource "aws_iam_policy" "replication-policy" {
  name        = var.replication-bucket-name
  description = "used for cross-region replication for security purposes."
  policy      = templatefile("${path.module}/template/replication_policy.json", {
    restricted-bucket = var.restricted-bucket-name
    replication-bucket = var.replication-bucket-name 
  })
}

resource "aws_iam_role_policy_attachment" "attachment" {
  role       = aws_iam_role.replication.name
  policy_arn = aws_iam_policy.replication-policy.arn  
}

resource "aws_s3_bucket_replication_configuration" "replication-config" {
  # Must have bucket versioning enabled first
  depends_on = [aws_s3_bucket_versioning.restricted_bucket_versioning]

  role   = aws_iam_role.replication.arn
  bucket = aws_s3_bucket.restricted_bucket.id

  rule {
    id = "Cross-region security replication"

    filter {
      prefix = "" 
    }

    status = "Enabled"

    destination {
      bucket        = var.replication-bucket-arn
      storage_class = "INTELLIGENT_TIERING"
    }
    delete_marker_replication {
      status = "Disabled" 
    }
  }
}


# TOSO Server access logging

resource "aws_s3_bucket_logging" "logging" {
  bucket = aws_s3_bucket.restricted_bucket.id

  target_bucket = var.log-bucket-name
  target_prefix = "s3accesslog/"

  target_object_key_format {
    partitioned_prefix {
      partition_date_source = "EventTime"
    }
  }
}