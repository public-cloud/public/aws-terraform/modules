variable "bucket_name" {
    type = string
    description = "Name of the bucket attached to transfer family"
}