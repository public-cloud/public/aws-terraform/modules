
# Create SFTP Server
resource "aws_transfer_server" "sftp_server" {
  identity_provider_type = "SERVICE_MANAGED"  # Based on the image
  endpoint_type          = "PUBLIC"
  protocols              = ["SFTP"]
  logging_role           = aws_iam_role.transfer_logging_role.arn
}

resource "aws_iam_role" "transfer_logging_role" {
  name = "aws_transfer_service_role"
  assume_role_policy = file("${path.module}/template/transfer_service_trust_relationship.json")  
}

resource "aws_iam_role_policy" "transfer_logging_role_policy" {
  name   = "aws_transfer_service_policy"
  role   = aws_iam_role.transfer_logging_role.name
  policy = templatefile("${path.module}/template/transfer_service_policy.json", {
    bucket_name = var.bucket_name
  })
}