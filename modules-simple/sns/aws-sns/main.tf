provider "aws" {
  region = var.aws_region
}

resource "aws_sns_topic" "sns_topic" {
    name = var.sns-topic-name
    kms_master_key_id = "alias/aws/sns"
    delivery_policy = file(var.sns-delivery-policy-file-path)
}

resource "aws_sns_topic_policy" "policy" {
    arn = aws_sns_topic.sns_topic.arn
    policy = file(var.sns-access-policy-file-path)
}

resource "aws_sns_topic_subscription" "email_subscription" {
    for_each = toset(var.email-subscriptions)
    topic_arn = aws_sns_topic.sns_topic.arn
    protocol = "email"
    endpoint = each.value
    confirmation_timeout_in_minutes = 3000
}