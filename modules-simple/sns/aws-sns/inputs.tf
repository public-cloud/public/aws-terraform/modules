variable "sns-topic-name" {
  type        = string
  description = "Name of the SNS topic"
}

variable "sns-access-policy-file-path" {
  type        = string
  description = "Path to the policy file"
  default = null
}

variable "sns-delivery-policy-file-path" {
  type = string
  description = "value of the SNS delivery policy file path"
  default = null
}

variable "aws_region" {
  type = string
  default = "us-east-2"
}

variable "email-subscriptions" {
  type        = list(string)
  description = "List of email addresses to subscribe to the SNS topic"
  default     = []
}