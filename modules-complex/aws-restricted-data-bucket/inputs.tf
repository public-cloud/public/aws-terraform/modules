variable "restricted-bucket-name" {
  type        = string
  description = "Name of the restricted data bucket"
}

variable "enable-bucket-versioning" {
  type        = string
  description = "Turn versioning on or off for the restricted bucket"
  default     = "Enabled"
}

variable "replication-bucket-name" {
  type    = string
  default = "Name of the bucket to replicate to"
}

variable "replication-bucket-region" {
  type = string
  default = "us-west-2"
}
variable "s3-event-notification-sns-topic-name-replication" {
  type = string
  default = "restricted-data-bucket-s3-event-notification-replication"
}
variable "sns-access-policy-replication-file-path" {
  type = string
  description = "value of the SNS policy file path"
}
variable "server-access-logging-bucket-name" {
  type        = string
  description = "Name of the bucket to store access logs"
}

variable "s3-event-notification-sns-topic-name" {
  type = string
  default = "restricted-data-bucket-s3-event-notification"
  description = "value of the SNS topic name"
}

variable "sns-access-policy-file-path" {
  type        = string
  description = "Path to the policy file"
  default = null
}

variable "sns-delivery-policy-file-path" {
  type = string
  description = "value of the SNS delivery policy file path"
  default = null
}

variable "email-subscriptions" {
  type        = list(string)
  description = "List of email addresses to subscribe to the SNS topic"
  default     = []
}

variable "sns-bucket-notification-topic-arn" {
  type        = string
  description = "arn of the SNS topic"
  default = null
}

variable "replication-lifecycle-rule-id" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Archive for cheaper storage"
}

variable "logging-lifecycle-rule-id" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Archive for cheaper storage"
}

variable "logging-deletion-lifecycle-rule-id" {
    type = string
    description = "value of the lifecycle rule id"
    default = "Deletion for cheaper storage"
}