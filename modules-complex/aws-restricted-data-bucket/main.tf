module "aws-sns-s3-event-notifications" {
  source = "../../modules-simple/sns/aws-sns"
  sns-topic-name = var.s3-event-notification-sns-topic-name
  sns-access-policy-file-path = var.sns-access-policy-file-path
  sns-delivery-policy-file-path = var.sns-delivery-policy-file-path
  email-subscriptions = var.email-subscriptions
}

module "aws-sns-s3-event-notifications-replication" {
  source = "../../modules-simple/sns/aws-sns"
  sns-topic-name = var.s3-event-notification-sns-topic-name-replication
  sns-access-policy-file-path = var.sns-access-policy-replication-file-path
  sns-delivery-policy-file-path = var.sns-delivery-policy-file-path
  email-subscriptions = var.email-subscriptions
  aws_region = var.replication-bucket-region
}

module "aws-s3-logging-bucket-restricted-data" {
  source = "../../modules-simple/s3/aws-s3-logging-bucket-restricted-data"
  server-access-logging-bucket-name = var.server-access-logging-bucket-name
  sns-bucket-notification-topic-arn =  module.aws-sns-s3-event-notifications.sns_topic_arn
  lifecycle-rule-id-cheaper = var.logging-lifecycle-rule-id
  lifecycle-rule-id-deletion = var.logging-deletion-lifecycle-rule-id
}

module "aws-s3-replication-bucket-restricted-data" {
  source = "../../modules-simple/s3/aws-s3-replication-bucket-restricted-data"
  replication-bucket-name = var.replication-bucket-name
  enable-bucket-versioning = var.enable-bucket-versioning
  sns-bucket-notification-topic-arn = module.aws-sns-s3-event-notifications-replication.sns_topic_arn
  lifecycle-rule-id = var.replication-lifecycle-rule-id
  replication-bucket-region = var.replication-bucket-region
}

module "aws-s3-bucket-restricted-data" {
  source = "../../modules-simple/s3/aws-s3-bucket-restricted-data"
  restricted-bucket-name    = var.restricted-bucket-name
  enable-bucket-versioning = var.enable-bucket-versioning
  sns-bucket-notification-topic-arn = module.aws-sns-s3-event-notifications.sns_topic_arn
  replication-bucket-name = module.aws-s3-replication-bucket-restricted-data.replication_bucket_name
  replication-bucket-arn = module.aws-s3-replication-bucket-restricted-data.replication_bucket_arn
  log-bucket-name = module.aws-s3-logging-bucket-restricted-data.log_bucket_name
}
